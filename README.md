Ansible Role to manage Telegraf installation
-------------------------------------------

This simple role allow you to install Telegraf on a specific machine.

## Example

```ansible-playbook
- name: Install Telegraf
  include_role:
    name: telegraf
  vars:
    influxdb_database: "{{ influxdb.database|default(ansible_host) }}"
    influxdb_url: "{{ influxdb.url }}"
    influxdb_username: "{{ influxdb.username }}"
    influxdb_password: "{{ influxdb.password }}"
```
